
This is the Linux source

"Makefile" is a simple make file to make zasm
"zasm.pro" is the Qt project for zasm
"config.h" contains platform specific settings

To build zasm you also need to clone the "Libraries/" from 
"https://megatokio@bitbucket.org/megatokio/libraries.git"
to "zasm/Libraries/". (uppercase 'L')
If your git client properly cloned the subproject it is already there.

