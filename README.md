# zasm

zasm - Z80 and 8080 assembler

You also need to checkout https://bitbucket.org/megatokio/Libraries. (uppercase 'L' - bitbucket sucks..) 
There should be a commit with tag *zasm*. 
Project *zasm* already contains a symlink to '../Libraries/'.

Project web page: [k1.spdns.de](https://k1.spdns.de/Develop/Projects/zasm/Distributions/).  
There you can download Binaries for OSX and Linux and some older versions for other OSes  
and there you find the [Documentation](https://k1.spdns.de/Develop/Projects/zasm/Documentation/) 
and an [online assembler](https://k1.spdns.de/cgi-bin/zasm.cgi).
