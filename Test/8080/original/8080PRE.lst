              	; --------------------------------------
              	; zasm: assemble "8080PRE.asm"
              	; opts: --asm8080
              	; date: 2018-05-18 20:50:25
              	; --------------------------------------


              	#!/usr/local/bin/zasm -o original/
              		title	'Preliminary Z80 tests'
              	
              	; prelim.z80 - Preliminary Z80 tests
              	; Copyright (C) 1994  Frank D. Cringle
              	;
              	; This program is free software; you can redistribute it and/or
              	; modify it under the terms of the GNU General Public License
              	; as published by the Free Software Foundation; either version 2
              	; of the License, or (at your option) any later version.
              	;
              	; This program is distributed in the hope that it will be useful,
              	; but WITHOUT ANY WARRANTY; without even the implied warranty of
              	; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
              	; GNU General Public License for more details.
              	;
              	; You should have received a copy of the GNU General Public License
              	; along with this program; if not, write to the Free Software
              	; Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
              	
              	
              	; These tests have two goals.  To start with, we assume the worst and
              	; successively test the instructions needed to continue testing.
              	; Then we try to test all instructions which cannot be handled by
              	; zexlax - the crc-based instruction exerciser.
              	
              	; Initially errors are 'reported' by jumping to 0.  This should reboot
              	; cp/m, so if the program terminates without any output one of the
              	; early tests failed.  Later errors are reported by outputting an
              	; address via the bdos conout routine.  The address can be located in
              	; a listing of this program.
              	
              	; If the program runs to completion it displays a suitable message.
              	
              	;******************************************************************************
              	;
              	; Modified by Ian Bartholomew to run a preliminary test on an 8080 CPU
              	;
              	; Assemble using M80
              	;
              	;******************************************************************************
              	
              		.8080
              		aseg
0100:         		org	100h
              	
0100: 3E01    	start:	mvi	a,1		; test simple compares and z/nz jumps
0102: FE02    		cpi	2
0104: CA0000  		jz	0
0107: FE01    		cpi	1
0109: C20000  		jnz	0
010C: C31101  		jmp	lab0
010F: 76      		hlt			; emergency exit
0110: FF      		db	0ffh
              		
0111: CD1701  	lab0:	call	lab2		; does a simple call work?
0114: C30000  	lab1:	jmp	0		; fail
              		
0117: E1      	lab2:	pop	h		; check return address
0118: 7C      		mov	a,h
0119: FE01    		cpi	hi(lab1)
011B: CA2101  		jz	lab3
011E: C30000  		jmp	0
0121: 7D      	lab3:	mov	a,l
0122: FE14    		cpi	lo(lab1)
0124: CA2A01  		jz	lab4
0127: C30000  		jmp	0
              	
              	; test presence and uniqueness of all machine registers
              	; (except ir)
012A: 319903  	lab4:	lxi	sp,regs1
012D: F1      		pop	psw
012E: C1      		pop	b
012F: D1      		pop	d
0130: E1      		pop	h
0131: 31A903  		lxi	sp,regs2+8
0134: E5      		push	h
0135: D5      		push	d
0136: C5      		push	b
0137: F5      		push	psw
              	
              	v	defl	0
              		rept	8
              		lda	regs2+v/2
              	v	defl	v+2
              		cpi	v
              		jnz	0
              		endm
0138: 3AA103  		lda	regs2+v/2
              	v	defl	v+2
013B: FE02    		cpi	v
013D: C20000  		jnz	0
0140: 3AA203  		lda	regs2+v/2
              	v	defl	v+2
0143: FE04    		cpi	v
0145: C20000  		jnz	0
0148: 3AA303  		lda	regs2+v/2
              	v	defl	v+2
014B: FE06    		cpi	v
014D: C20000  		jnz	0
0150: 3AA403  		lda	regs2+v/2
              	v	defl	v+2
0153: FE08    		cpi	v
0155: C20000  		jnz	0
0158: 3AA503  		lda	regs2+v/2
              	v	defl	v+2
015B: FE0A    		cpi	v
015D: C20000  		jnz	0
0160: 3AA603  		lda	regs2+v/2
              	v	defl	v+2
0163: FE0C    		cpi	v
0165: C20000  		jnz	0
0168: 3AA703  		lda	regs2+v/2
              	v	defl	v+2
016B: FE0E    		cpi	v
016D: C20000  		jnz	0
0170: 3AA803  		lda	regs2+v/2
              	v	defl	v+2
0173: FE10    		cpi	v
0175: C20000  		jnz	0
              	
              	; test access to memory via (hl)
0178: 21A903  		lxi	h,hlval
017B: 7E      		mov	a,m
017C: FEA5    		cpi	0a5h
017E: C20000  		jnz	0
0181: 21AA03  		lxi	h,hlval+1
0184: 7E      		mov	a,m
0185: FE3C    		cpi	03ch
0187: C20000  		jnz	0
              	
              	; test unconditional return
018A: 310005  		lxi	sp,stack
018D: 219501  		lxi	h,reta
0190: E5      		push	h
0191: C9      		ret
0192: C30000  		jmp	0
              	
              	; test instructions needed for hex output
0195: 3EFF    	reta:	mvi	a,0ffh
0197: E60F    		ani	0fh
0199: FE0F    		cpi	0fh
019B: C20000  		jnz	0
019E: 3E5A    		mvi	a,05ah
01A0: E60F    		ani	0fh
01A2: FE0A    		cpi	0ah
01A4: C20000  		jnz	0
01A7: 0F      		rrc
01A8: FE05    		cpi	05h
01AA: C20000  		jnz	0
01AD: 0F      		rrc
01AE: FE82    		cpi	82h
01B0: C20000  		jnz	0
01B3: 0F      		rrc
01B4: FE41    		cpi	41h
01B6: C20000  		jnz	0
01B9: 0F      		rrc
01BA: FEA0    		cpi	0a0h
01BC: C20000  		jnz	0
01BF: 213412  		lxi	h,01234h
01C2: E5      		push	h
01C3: C1      		pop	b
01C4: 78      		mov	a,b
01C5: FE12    		cpi	12h
01C7: C20000  		jnz	0
01CA: 79      		mov	a,c
01CB: FE34    		cpi	34h
01CD: C20000  		jnz	0
              		
              	; from now on we can report errors by displaying an address
              	
              	; test conditional call, ret, jp, jr
              	tcond	macro	flag,pcond,ncond,rel
              		lxi	h,&flag
              		push	h
              		pop	psw
              		c&pcond	lab1&pcond
              		jmp	error
              	lab1&pcond:	pop	h
              		lxi	h,0d7h xor &flag
              		push	h
              		pop	psw
              		c&ncond	lab2&pcond
              		jmp	error
              	lab2&pcond:	pop	h
              		lxi	h,lab3&pcond
              		push	h
              		lxi	h,&flag
              		push	h
              		pop	psw
              		r&pcond
              		call	error
              	lab3&pcond:	lxi	h,lab4&pcond
              		push	h
              		lxi	h,0d7h xor &flag
              		push	h
              		pop	psw
              		r&ncond
              		call	error
              	lab4&pcond:	lxi	h,&flag
              		push	h
              		pop	psw
              		j&pcond	lab5&pcond
              		call	error
              	lab5&pcond:	lxi	h,0d7h xor &flag
              		push	h
              		pop	psw
              		j&ncond	lab6&pcond
              		call	error
              	lab6&pcond:	
              		endm
              	
              		tcond	1,c,nc,1
01D0: 210100  		lxi	h,1
01D3: E5      		push	h
01D4: F1      		pop	psw
01D5: DCDB01  		cc	lab1c
01D8: C35203  		jmp	error
01DB: E1      	lab1c:	pop	h
01DC: 21D600  		lxi	h,0d7h xor 1
01DF: E5      		push	h
01E0: F1      		pop	psw
01E1: D4E701  		cnc	lab2c
01E4: C35203  		jmp	error
01E7: E1      	lab2c:	pop	h
01E8: 21F501  		lxi	h,lab3c
01EB: E5      		push	h
01EC: 210100  		lxi	h,1
01EF: E5      		push	h
01F0: F1      		pop	psw
01F1: D8      		rc
01F2: CD5203  		call	error
01F5: 210202  	lab3c:	lxi	h,lab4c
01F8: E5      		push	h
01F9: 21D600  		lxi	h,0d7h xor 1
01FC: E5      		push	h
01FD: F1      		pop	psw
01FE: D0      		rnc
01FF: CD5203  		call	error
0202: 210100  	lab4c:	lxi	h,1
0205: E5      		push	h
0206: F1      		pop	psw
0207: DA0D02  		jc	lab5c
020A: CD5203  		call	error
020D: 21D600  	lab5c:	lxi	h,0d7h xor 1
0210: E5      		push	h
0211: F1      		pop	psw
0212: D21802  		jnc	lab6c
0215: CD5203  		call	error
0218:         	lab6c:	
              		tcond	4,pe,po,0
0218: 210400  		lxi	h,4
021B: E5      		push	h
021C: F1      		pop	psw
021D: EC2302  		cpe	lab1pe
0220: C35203  		jmp	error
0223: E1      	lab1pe:	pop	h
0224: 21D300  		lxi	h,0d7h xor 4
0227: E5      		push	h
0228: F1      		pop	psw
0229: E42F02  		cpo	lab2pe
022C: C35203  		jmp	error
022F: E1      	lab2pe:	pop	h
0230: 213D02  		lxi	h,lab3pe
0233: E5      		push	h
0234: 210400  		lxi	h,4
0237: E5      		push	h
0238: F1      		pop	psw
0239: E8      		rpe
023A: CD5203  		call	error
023D: 214A02  	lab3pe:	lxi	h,lab4pe
0240: E5      		push	h
0241: 21D300  		lxi	h,0d7h xor 4
0244: E5      		push	h
0245: F1      		pop	psw
0246: E0      		rpo
0247: CD5203  		call	error
024A: 210400  	lab4pe:	lxi	h,4
024D: E5      		push	h
024E: F1      		pop	psw
024F: EA5502  		jpe	lab5pe
0252: CD5203  		call	error
0255: 21D300  	lab5pe:	lxi	h,0d7h xor 4
0258: E5      		push	h
0259: F1      		pop	psw
025A: E26002  		jpo	lab6pe
025D: CD5203  		call	error
0260:         	lab6pe:	
              		tcond	040h,z,nz,1
0260: 214000  		lxi	h,040h
0263: E5      		push	h
0264: F1      		pop	psw
0265: CC6B02  		cz	lab1z
0268: C35203  		jmp	error
026B: E1      	lab1z:	pop	h
026C: 219700  		lxi	h,0d7h xor 040h
026F: E5      		push	h
0270: F1      		pop	psw
0271: C47702  		cnz	lab2z
0274: C35203  		jmp	error
0277: E1      	lab2z:	pop	h
0278: 218502  		lxi	h,lab3z
027B: E5      		push	h
027C: 214000  		lxi	h,040h
027F: E5      		push	h
0280: F1      		pop	psw
0281: C8      		rz
0282: CD5203  		call	error
0285: 219202  	lab3z:	lxi	h,lab4z
0288: E5      		push	h
0289: 219700  		lxi	h,0d7h xor 040h
028C: E5      		push	h
028D: F1      		pop	psw
028E: C0      		rnz
028F: CD5203  		call	error
0292: 214000  	lab4z:	lxi	h,040h
0295: E5      		push	h
0296: F1      		pop	psw
0297: CA9D02  		jz	lab5z
029A: CD5203  		call	error
029D: 219700  	lab5z:	lxi	h,0d7h xor 040h
02A0: E5      		push	h
02A1: F1      		pop	psw
02A2: C2A802  		jnz	lab6z
02A5: CD5203  		call	error
02A8:         	lab6z:	
              		tcond	080h,m,p,0
02A8: 218000  		lxi	h,080h
02AB: E5      		push	h
02AC: F1      		pop	psw
02AD: FCB302  		cm	lab1m
02B0: C35203  		jmp	error
02B3: E1      	lab1m:	pop	h
02B4: 215700  		lxi	h,0d7h xor 080h
02B7: E5      		push	h
02B8: F1      		pop	psw
02B9: F4BF02  		cp	lab2m
02BC: C35203  		jmp	error
02BF: E1      	lab2m:	pop	h
02C0: 21CD02  		lxi	h,lab3m
02C3: E5      		push	h
02C4: 218000  		lxi	h,080h
02C7: E5      		push	h
02C8: F1      		pop	psw
02C9: F8      		rm
02CA: CD5203  		call	error
02CD: 21DA02  	lab3m:	lxi	h,lab4m
02D0: E5      		push	h
02D1: 215700  		lxi	h,0d7h xor 080h
02D4: E5      		push	h
02D5: F1      		pop	psw
02D6: F0      		rp
02D7: CD5203  		call	error
02DA: 218000  	lab4m:	lxi	h,080h
02DD: E5      		push	h
02DE: F1      		pop	psw
02DF: FAE502  		jm	lab5m
02E2: CD5203  		call	error
02E5: 215700  	lab5m:	lxi	h,0d7h xor 080h
02E8: E5      		push	h
02E9: F1      		pop	psw
02EA: F2F002  		jp	lab6m
02ED: CD5203  		call	error
02F0:         	lab6m:	
              	
              	; test indirect jumps
02F0: 21F702  		lxi	h,lab7
02F3: E9      		pchl
02F4: CD5203  		call	error
              	
              	; djnz (and (partially) inc a, inc hl)
02F7: 3EA5    	lab7:	mvi	a,0a5h
02F9: 0604    		mvi	b,4
02FB: 0F      	lab8:	rrc
02FC: 05      		dcr	b
02FD: C2FB02  		jnz	lab8
0300: FE5A    		cpi	05ah
0302: C45203  		cnz	error
0305: 0610    		mvi	b,16
0307: 3C      	lab9:	inr	a
0308: 05      		dcr	b
0309: C20703  		jnz	lab9
030C: FE6A    		cpi	06ah
030E: C45203  		cnz	error
0311: 0600    		mvi	b,0
0313: 210000  		lxi	h,0
0316: 23      	lab10:	inx	h
0317: 05      		dcr	b
0318: C21603  		jnz	lab10
031B: 7C      		mov	a,h
031C: FE01    		cpi	1
031E: C45203  		cnz	error
0321: 7D      		mov	a,l
0322: FE00    		cpi	0
0324: C45203  		cnz	error
              		
0327: 113203  	allok:	lxi	d,okmsg
032A: 0E09    		mvi	c,9
032C: CD0500  		call	5
032F: C30000  		jmp	0
              	
0332: 38303830	okmsg:	db	'8080 Preliminary tests complete$'
0336: 20507265	
033A: 6C696D69	
033E: 6E617279	
0342: 20746573	
0346: 74732063	
034A: 6F6D706C	
034E: 65746524	
              		
              	; display address at top of stack and exit
0352: C1      	error:	pop	b
0353: 2604    		mvi	h,hi(hextab)
0355: 78      		mov	a,b
0356: 0F      		rrc
0357: 0F      		rrc
0358: 0F      		rrc
0359: 0F      		rrc
035A: E60F    		ani	15
035C: 6F      		mov	l,a
035D: 7E      		mov	a,m
035E: CD8A03  		call	conout
0361: 78      		mov	a,b
0362: E60F    		ani	15
0364: 6F      		mov	l,a
0365: 7E      		mov	a,m
0366: CD8A03  		call	conout
0369: 79      		mov	a,c
036A: 0F      		rrc
036B: 0F      		rrc
036C: 0F      		rrc
036D: 0F      		rrc
036E: E60F    		ani	15
0370: 6F      		mov	l,a
0371: 7E      		mov	a,m
0372: CD8A03  		call	conout
0375: 79      		mov	a,c
0376: E60F    		ani	15
0378: 6F      		mov	l,a
0379: 7E      		mov	a,m
037A: CD8A03  		call	conout
037D: 3E0D    		mvi	a,13
037F: CD8A03  		call	conout
0382: 3E0A    		mvi	a,10
0384: CD8A03  		call	conout
0387: C30000  		jmp	0
              	
038A: F5      	conout:	push	psw
038B: C5      		push	b
038C: D5      		push	d
038D: E5      		push	h
038E: 0E02    		mvi	c,2
0390: 5F      		mov	e,a
0391: CD0500  		call	5
0394: E1      		pop	h
0395: D1      		pop	d
0396: C1      		pop	b
0397: F1      		pop	psw
0398: C9      		ret
              		
              	v	defl	0
0399:         	regs1:	rept	8
              	v	defl	v+2
              		db	v
              		endm
              	v	defl	v+2
0399: 02      		db	v
              	v	defl	v+2
039A: 04      		db	v
              	v	defl	v+2
039B: 06      		db	v
              	v	defl	v+2
039C: 08      		db	v
              	v	defl	v+2
039D: 0A      		db	v
              	v	defl	v+2
039E: 0C      		db	v
              	v	defl	v+2
039F: 0E      		db	v
              	v	defl	v+2
03A0: 10      		db	v
              	
03A1: 00000000	regs2:	ds	8,0
03A5: 00000000	
              	
03A9: A53C    	hlval:	db	0a5h,03ch
              	
              	; skip to next page boundary
03AB: FFFFFFFF		org	(($+255)/256)*256
03AF: FF...   	
0400: 30313233	hextab:	db	'0123456789abcdef'
0404: 34353637	
0408: 38396162	
040C: 63646566	
0410: FFFFFFFF		ds	240
0414: FF...   	
              	
0500:         	stack	equ	$
              	
              		end	start
***ERROR*** No such file or directory: file = "/pub/Develop/Projects/zasm-4.0/Test/8080/original/8080PRE." (fd108)


; +++ segments +++

#CODE          = $0100 =   256,  size = $0400 =  1024

; +++ global symbols +++

_8080_    = $0001 =     1          8080PRE.asm:43 (unused)
_asm8080_ = $0001 =     1          8080PRE.asm:43 (unused)
_end      = $0500 =  1280          8080PRE.asm:44 (unused)
_size     = $0400 =  1024          8080PRE.asm:44 (unused)
allok     = $0327 =   807          8080PRE.asm:217 (unused)
conout    = $038A =   906          8080PRE.asm:261
error     = $0352 =   850          8080PRE.asm:225
hextab    = $0400 =  1024          8080PRE.asm:286
hlval     = $03A9 =   937          8080PRE.asm:282
lab0      = $0111 =   273          8080PRE.asm:56
lab1      = $0114 =   276          8080PRE.asm:57
lab10     = $0316 =   790          8080PRE.asm:207
lab1c     = $01DB =   475          8080PRE.asm:147
lab1m     = $02B3 =   691          8080PRE.asm:147
lab1pe    = $0223 =   547          8080PRE.asm:147
lab1z     = $026B =   619          8080PRE.asm:147
lab2      = $0117 =   279          8080PRE.asm:59
lab2c     = $01E7 =   487          8080PRE.asm:153
lab2m     = $02BF =   703          8080PRE.asm:153
lab2pe    = $022F =   559          8080PRE.asm:153
lab2z     = $0277 =   631          8080PRE.asm:153
lab3      = $0121 =   289          8080PRE.asm:64
lab3c     = $01F5 =   501          8080PRE.asm:161
lab3m     = $02CD =   717          8080PRE.asm:161
lab3pe    = $023D =   573          8080PRE.asm:161
lab3z     = $0285 =   645          8080PRE.asm:161
lab4      = $012A =   298          8080PRE.asm:71
lab4c     = $0202 =   514          8080PRE.asm:168
lab4m     = $02DA =   730          8080PRE.asm:168
lab4pe    = $024A =   586          8080PRE.asm:168
lab4z     = $0292 =   658          8080PRE.asm:168
lab5c     = $020D =   525          8080PRE.asm:173
lab5m     = $02E5 =   741          8080PRE.asm:173
lab5pe    = $0255 =   597          8080PRE.asm:173
lab5z     = $029D =   669          8080PRE.asm:173
lab6c     = $0218 =   536          8080PRE.asm:178
lab6m     = $02F0 =   752          8080PRE.asm:178
lab6pe    = $0260 =   608          8080PRE.asm:178
lab6z     = $02A8 =   680          8080PRE.asm:178
lab7      = $02F7 =   759          8080PRE.asm:192
lab8      = $02FB =   763          8080PRE.asm:194
lab9      = $0307 =   775          8080PRE.asm:200
okmsg     = $0332 =   818          8080PRE.asm:222
regs1     = $0399 =   921          8080PRE.asm:275
regs2     = $03A1 =   929          8080PRE.asm:280
reta      = $0195 =   405          8080PRE.asm:108
stack     = $0500 =  1280          8080PRE.asm:289
start     = $0100 =   256          8080PRE.asm:47 (unused)
v         = $0010 =    16          8080PRE.asm:82


total time: 178.1602 sec.
1 error
